﻿namespace FFMPEGConverter
{
	partial class ConfigForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.yesButton = new System.Windows.Forms.Button();
			this.noButton = new System.Windows.Forms.Button();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.ffmpegPath = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.outputPath = new System.Windows.Forms.TextBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.deleteFile = new System.Windows.Forms.CheckBox();
			this.parameterOutput = new System.Windows.Forms.CheckBox();
			this.overrideFile = new System.Windows.Forms.CheckBox();
			this.videoCRF = new System.Windows.Forms.NumericUpDown();
			this.videoCustom = new System.Windows.Forms.TextBox();
			this.audioBitrate = new System.Windows.Forms.NumericUpDown();
			this.audioCutoff = new System.Windows.Forms.NumericUpDown();
			this.audioCustom = new System.Windows.Forms.TextBox();
			this.audioMixdown = new System.Windows.Forms.CheckBox();
			this.videoFormats = new System.Windows.Forms.TextBox();
			this.audioFormats = new System.Windows.Forms.TextBox();
			this.cmdOutput = new System.Windows.Forms.CheckBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.videoPreset = new System.Windows.Forms.ComboBox();
			this.videoCodec = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.audioCodec = new System.Windows.Forms.ComboBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.outputFormat = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.videoCRF)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.audioBitrate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.audioCutoff)).BeginInit();
			this.SuspendLayout();
			// 
			// yesButton
			// 
			this.yesButton.Location = new System.Drawing.Point(16, 470);
			this.yesButton.Margin = new System.Windows.Forms.Padding(4);
			this.yesButton.Name = "yesButton";
			this.yesButton.Size = new System.Drawing.Size(132, 42);
			this.yesButton.TabIndex = 0;
			this.yesButton.Text = "Confirm";
			this.yesButton.UseVisualStyleBackColor = true;
			this.yesButton.Click += new System.EventHandler(this.yesButton_Click);
			// 
			// noButton
			// 
			this.noButton.Location = new System.Drawing.Point(497, 470);
			this.noButton.Margin = new System.Windows.Forms.Padding(4);
			this.noButton.Name = "noButton";
			this.noButton.Size = new System.Drawing.Size(132, 42);
			this.noButton.TabIndex = 1;
			this.noButton.Text = "Cancel";
			this.noButton.UseVisualStyleBackColor = true;
			this.noButton.Click += new System.EventHandler(this.noButton_Click);
			// 
			// folderBrowserDialog1
			// 
			this.folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer;
			this.folderBrowserDialog1.ShowNewFolderButton = false;
			// 
			// ffmpegPath
			// 
			this.ffmpegPath.Location = new System.Drawing.Point(156, 50);
			this.ffmpegPath.Margin = new System.Windows.Forms.Padding(4);
			this.ffmpegPath.Name = "ffmpegPath";
			this.ffmpegPath.ReadOnly = true;
			this.ffmpegPath.Size = new System.Drawing.Size(219, 22);
			this.ffmpegPath.TabIndex = 2;
			this.toolTip1.SetToolTip(this.ffmpegPath, "ffmpeg.exe and ffprobe.exe");
			this.ffmpegPath.Click += new System.EventHandler(this.ffmpegPath_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 11);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(106, 29);
			this.label1.TabIndex = 3;
			this.label1.Text = "General";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 47);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(136, 28);
			this.label2.TabIndex = 0;
			this.label2.Text = "FFmpeg Path";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(12, 80);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(136, 28);
			this.label3.TabIndex = 1;
			this.label3.Text = "Output Path";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// outputPath
			// 
			this.outputPath.Location = new System.Drawing.Point(156, 82);
			this.outputPath.Margin = new System.Windows.Forms.Padding(4);
			this.outputPath.Name = "outputPath";
			this.outputPath.ReadOnly = true;
			this.outputPath.Size = new System.Drawing.Size(219, 22);
			this.outputPath.TabIndex = 3;
			this.toolTip1.SetToolTip(this.outputPath, "Empty == Source Path");
			this.outputPath.Click += new System.EventHandler(this.outputPath_Click);
			this.outputPath.KeyUp += new System.Windows.Forms.KeyEventHandler(this.outputPath_KeyUp);
			// 
			// deleteFile
			// 
			this.deleteFile.AutoSize = true;
			this.deleteFile.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.deleteFile.Location = new System.Drawing.Point(384, 71);
			this.deleteFile.Margin = new System.Windows.Forms.Padding(4);
			this.deleteFile.Name = "deleteFile";
			this.deleteFile.Size = new System.Drawing.Size(141, 21);
			this.deleteFile.TabIndex = 4;
			this.deleteFile.Text = "Delete after finish";
			this.toolTip1.SetToolTip(this.deleteFile, "File delete after convert complete");
			this.deleteFile.UseVisualStyleBackColor = true;
			this.deleteFile.CheckedChanged += new System.EventHandler(this.deleteFile_CheckedChanged);
			// 
			// parameterOutput
			// 
			this.parameterOutput.AutoSize = true;
			this.parameterOutput.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.parameterOutput.Location = new System.Drawing.Point(384, 47);
			this.parameterOutput.Margin = new System.Windows.Forms.Padding(4);
			this.parameterOutput.Name = "parameterOutput";
			this.parameterOutput.Size = new System.Drawing.Size(140, 21);
			this.parameterOutput.TabIndex = 5;
			this.parameterOutput.Text = "Parameter output";
			this.toolTip1.SetToolTip(this.parameterOutput, "Convert Parameter will be shown in error output");
			this.parameterOutput.UseVisualStyleBackColor = true;
			this.parameterOutput.CheckedChanged += new System.EventHandler(this.parameterOutput_CheckedChanged);
			// 
			// overrideFile
			// 
			this.overrideFile.AutoSize = true;
			this.overrideFile.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.overrideFile.Location = new System.Drawing.Point(531, 47);
			this.overrideFile.Margin = new System.Windows.Forms.Padding(4);
			this.overrideFile.Name = "overrideFile";
			this.overrideFile.Size = new System.Drawing.Size(107, 21);
			this.overrideFile.TabIndex = 6;
			this.overrideFile.Text = "Override file";
			this.toolTip1.SetToolTip(this.overrideFile, "Override if output file already exist");
			this.overrideFile.UseVisualStyleBackColor = true;
			this.overrideFile.CheckedChanged += new System.EventHandler(this.overrideFile_CheckedChanged);
			// 
			// videoCRF
			// 
			this.videoCRF.Location = new System.Drawing.Point(132, 217);
			this.videoCRF.Margin = new System.Windows.Forms.Padding(4);
			this.videoCRF.Maximum = new decimal(new int[] {
            51,
            0,
            0,
            0});
			this.videoCRF.Name = "videoCRF";
			this.videoCRF.Size = new System.Drawing.Size(67, 22);
			this.videoCRF.TabIndex = 13;
			this.toolTip1.SetToolTip(this.videoCRF, "Constant Rate Factor");
			this.videoCRF.ValueChanged += new System.EventHandler(this.videoCRF_ValueChanged);
			// 
			// videoCustom
			// 
			this.videoCustom.Location = new System.Drawing.Point(132, 249);
			this.videoCustom.Margin = new System.Windows.Forms.Padding(4);
			this.videoCustom.Name = "videoCustom";
			this.videoCustom.Size = new System.Drawing.Size(243, 22);
			this.videoCustom.TabIndex = 16;
			this.toolTip1.SetToolTip(this.videoCustom, "ffmpeg.exe and ffprobe.exe");
			this.videoCustom.TextChanged += new System.EventHandler(this.videoCustom_TextChanged);
			// 
			// audioBitrate
			// 
			this.audioBitrate.Location = new System.Drawing.Point(135, 350);
			this.audioBitrate.Margin = new System.Windows.Forms.Padding(4);
			this.audioBitrate.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
			this.audioBitrate.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.audioBitrate.Name = "audioBitrate";
			this.audioBitrate.Size = new System.Drawing.Size(100, 22);
			this.audioBitrate.TabIndex = 20;
			this.toolTip1.SetToolTip(this.audioBitrate, "Bitrate in K");
			this.audioBitrate.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.audioBitrate.ValueChanged += new System.EventHandler(this.audioBitrate_ValueChanged);
			// 
			// audioCutoff
			// 
			this.audioCutoff.Location = new System.Drawing.Point(135, 378);
			this.audioCutoff.Margin = new System.Windows.Forms.Padding(4);
			this.audioCutoff.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
			this.audioCutoff.Name = "audioCutoff";
			this.audioCutoff.Size = new System.Drawing.Size(100, 22);
			this.audioCutoff.TabIndex = 22;
			this.toolTip1.SetToolTip(this.audioCutoff, "Frequency cutoff ( 0 == no cutoff )");
			this.audioCutoff.ValueChanged += new System.EventHandler(this.audioCutoff_ValueChanged);
			// 
			// audioCustom
			// 
			this.audioCustom.Location = new System.Drawing.Point(135, 410);
			this.audioCustom.Margin = new System.Windows.Forms.Padding(4);
			this.audioCustom.Name = "audioCustom";
			this.audioCustom.Size = new System.Drawing.Size(243, 22);
			this.audioCustom.TabIndex = 24;
			this.toolTip1.SetToolTip(this.audioCustom, "ffmpeg.exe and ffprobe.exe");
			this.audioCustom.TextChanged += new System.EventHandler(this.audioCustom_TextChanged);
			// 
			// audioMixdown
			// 
			this.audioMixdown.AutoSize = true;
			this.audioMixdown.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.audioMixdown.Location = new System.Drawing.Point(68, 442);
			this.audioMixdown.Margin = new System.Windows.Forms.Padding(4);
			this.audioMixdown.Name = "audioMixdown";
			this.audioMixdown.Size = new System.Drawing.Size(83, 21);
			this.audioMixdown.TabIndex = 26;
			this.audioMixdown.Text = "Mixdown";
			this.toolTip1.SetToolTip(this.audioMixdown, "Mixdown > 2 Channels to Stereo");
			this.audioMixdown.UseVisualStyleBackColor = true;
			this.audioMixdown.CheckedChanged += new System.EventHandler(this.audioMixdown_CheckedChanged);
			// 
			// videoFormats
			// 
			this.videoFormats.Location = new System.Drawing.Point(397, 154);
			this.videoFormats.Margin = new System.Windows.Forms.Padding(4);
			this.videoFormats.Multiline = true;
			this.videoFormats.Name = "videoFormats";
			this.videoFormats.Size = new System.Drawing.Size(231, 137);
			this.videoFormats.TabIndex = 27;
			this.toolTip1.SetToolTip(this.videoFormats, "This formats will be marked to convert on added");
			// 
			// audioFormats
			// 
			this.audioFormats.Location = new System.Drawing.Point(397, 321);
			this.audioFormats.Margin = new System.Windows.Forms.Padding(4);
			this.audioFormats.Multiline = true;
			this.audioFormats.Name = "audioFormats";
			this.audioFormats.Size = new System.Drawing.Size(231, 137);
			this.audioFormats.TabIndex = 29;
			this.toolTip1.SetToolTip(this.audioFormats, "This formats will be marked to convert on added");
			// 
			// cmdOutput
			// 
			this.cmdOutput.AutoSize = true;
			this.cmdOutput.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.cmdOutput.Location = new System.Drawing.Point(527, 71);
			this.cmdOutput.Margin = new System.Windows.Forms.Padding(4);
			this.cmdOutput.Name = "cmdOutput";
			this.cmdOutput.Size = new System.Drawing.Size(107, 21);
			this.cmdOutput.TabIndex = 31;
			this.cmdOutput.Text = "CMD Output";
			this.toolTip1.SetToolTip(this.cmdOutput, "Show cmd output in error window");
			this.cmdOutput.UseVisualStyleBackColor = true;
			this.cmdOutput.CheckedChanged += new System.EventHandler(this.cmdOutput_CheckedChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(16, 118);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(81, 29);
			this.label4.TabIndex = 7;
			this.label4.Text = "Video";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 154);
			this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(108, 28);
			this.label5.TabIndex = 8;
			this.label5.Text = "Codec";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(19, 182);
			this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(105, 28);
			this.label6.TabIndex = 10;
			this.label6.Text = "Preset";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// videoPreset
			// 
			this.videoPreset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.videoPreset.FormattingEnabled = true;
			this.videoPreset.Items.AddRange(new object[] {
            "ultrafast",
            "superfast",
            "veryfast",
            "faster",
            "fast",
            "medium",
            "slow",
            "slower",
            "veryslow",
            "placebo"});
			this.videoPreset.Location = new System.Drawing.Point(132, 185);
			this.videoPreset.Margin = new System.Windows.Forms.Padding(4);
			this.videoPreset.Name = "videoPreset";
			this.videoPreset.Size = new System.Drawing.Size(136, 24);
			this.videoPreset.TabIndex = 11;
			this.videoPreset.SelectedIndexChanged += new System.EventHandler(this.videoPreset_SelectedIndexChanged);
			// 
			// videoCodec
			// 
			this.videoCodec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.videoCodec.FormattingEnabled = true;
			this.videoCodec.Items.AddRange(new object[] {
            "libx264",
            "libx265"});
			this.videoCodec.Location = new System.Drawing.Point(132, 154);
			this.videoCodec.Margin = new System.Windows.Forms.Padding(4);
			this.videoCodec.Name = "videoCodec";
			this.videoCodec.Size = new System.Drawing.Size(136, 24);
			this.videoCodec.TabIndex = 12;
			this.videoCodec.SelectedIndexChanged += new System.EventHandler(this.videoCodec_SelectedIndexChanged);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(19, 217);
			this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(105, 28);
			this.label7.TabIndex = 14;
			this.label7.Text = "CRF";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(16, 286);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(80, 29);
			this.label8.TabIndex = 15;
			this.label8.Text = "Audio";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(19, 245);
			this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(108, 28);
			this.label9.TabIndex = 17;
			this.label9.Text = "Custom";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// audioCodec
			// 
			this.audioCodec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.audioCodec.FormattingEnabled = true;
			this.audioCodec.Items.AddRange(new object[] {
            "libfdk_aac",
            "ac3",
            "eac3",
            "wmav1",
            "wmav2",
            "vorbis",
            "libshine",
            "libopus",
            "mp2"});
			this.audioCodec.Location = new System.Drawing.Point(135, 321);
			this.audioCodec.Margin = new System.Windows.Forms.Padding(4);
			this.audioCodec.Name = "audioCodec";
			this.audioCodec.Size = new System.Drawing.Size(136, 24);
			this.audioCodec.TabIndex = 19;
			this.audioCodec.SelectedIndexChanged += new System.EventHandler(this.audioCodec_SelectedIndexChanged);
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(19, 321);
			this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(108, 28);
			this.label10.TabIndex = 18;
			this.label10.Text = "Codec";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(21, 350);
			this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(105, 28);
			this.label11.TabIndex = 21;
			this.label11.Text = "Bitrate";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(21, 378);
			this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(105, 28);
			this.label12.TabIndex = 23;
			this.label12.Text = "Cutoff";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(21, 406);
			this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(108, 28);
			this.label13.TabIndex = 25;
			this.label13.Text = "Custom";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(371, 126);
			this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(108, 28);
			this.label14.TabIndex = 28;
			this.label14.Text = "Formats";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(371, 293);
			this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(108, 28);
			this.label15.TabIndex = 30;
			this.label15.Text = "Formats";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// outputFormat
			// 
			this.outputFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.outputFormat.FormattingEnabled = true;
			this.outputFormat.Items.AddRange(new object[] {
            "mkv",
            "aac",
            "mp3",
            "mp4"});
			this.outputFormat.Location = new System.Drawing.Point(388, 98);
			this.outputFormat.Margin = new System.Windows.Forms.Padding(4);
			this.outputFormat.Name = "outputFormat";
			this.outputFormat.Size = new System.Drawing.Size(136, 24);
			this.outputFormat.TabIndex = 32;
			this.outputFormat.SelectedIndexChanged += new System.EventHandler(this.outputFormat_SelectedIndexChanged);
			// 
			// ConfigForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(645, 522);
			this.ControlBox = false;
			this.Controls.Add(this.outputFormat);
			this.Controls.Add(this.cmdOutput);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.audioFormats);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.videoFormats);
			this.Controls.Add(this.audioMixdown);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.audioCustom);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.audioCutoff);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.audioBitrate);
			this.Controls.Add(this.audioCodec);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.videoCustom);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.videoCRF);
			this.Controls.Add(this.videoCodec);
			this.Controls.Add(this.videoPreset);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.overrideFile);
			this.Controls.Add(this.parameterOutput);
			this.Controls.Add(this.outputPath);
			this.Controls.Add(this.deleteFile);
			this.Controls.Add(this.ffmpegPath);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.noButton);
			this.Controls.Add(this.yesButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ConfigForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "ConfigForm";
			this.TopMost = true;
			((System.ComponentModel.ISupportInitialize)(this.videoCRF)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.audioBitrate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.audioCutoff)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button yesButton;
		private System.Windows.Forms.Button noButton;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.TextBox ffmpegPath;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox outputPath;
		private System.Windows.Forms.CheckBox deleteFile;
		private System.Windows.Forms.CheckBox parameterOutput;
		private System.Windows.Forms.CheckBox overrideFile;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox videoPreset;
		private System.Windows.Forms.ComboBox videoCodec;
		private System.Windows.Forms.NumericUpDown videoCRF;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox videoCustom;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ComboBox audioCodec;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.NumericUpDown audioBitrate;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.NumericUpDown audioCutoff;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox audioCustom;
		private System.Windows.Forms.CheckBox audioMixdown;
		private System.Windows.Forms.TextBox videoFormats;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox audioFormats;
		private System.Windows.Forms.CheckBox cmdOutput;
		private System.Windows.Forms.ComboBox outputFormat;
	}
}