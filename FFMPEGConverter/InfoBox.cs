﻿using System;
using System.Windows.Forms;

namespace FFMPEGConverter
{
	public partial class InfoBox : Form
	{
		private const int   MAXLINES = 500;
		private string[]		CopyStorage = new string[MAXLINES]; 

		public InfoBox( Form1.Entry InfoStruct )
		{
			InitializeComponent();

			if( InfoStruct.VideoStreams != null )
			{
				textBox.Text = "VideoStreams : \n";
				PrintStreamInfo( InfoStruct.VideoStreams );
			}
			if( InfoStruct.AudioStreams != null )
			{
				textBox.Text += "\nAudioStreams : \n";
				PrintStreamInfo( InfoStruct.AudioStreams );
			}

			Text = InfoStruct.Filepath;
		}

		public InfoBox()
		{
			InitializeComponent();

			Text = "Output";
		}

		public void AddErrorLine( string Message )
		{
			if( textBox.Lines.Length > MAXLINES )
			{
				Array.Copy( textBox.Lines, textBox.Lines.Length - MAXLINES, CopyStorage, 0, MAXLINES );
				textBox.Lines = CopyStorage;
				textBox.SelectionStart = textBox.GetFirstCharIndexFromLine( textBox.Lines.Length - 1 );
				textBox.ScrollToCaret();
			}

			if( Message != null )
				textBox.AppendText( Message + '\n' );
		}

		private void PrintStreamInfo( string[][] StreamArray )
		{
			foreach( string[] StreamInfo in StreamArray )
			{
				foreach( string InfoLine in StreamInfo )
				{
					if( InfoLine == string.Empty ) continue;

					AddErrorLine( InfoLine );
				}
			}
		}
	}
}
