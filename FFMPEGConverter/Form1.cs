﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Forms;

namespace FFMPEGConverter
{
	public partial class Form1 : Form
	{
		private List<Entry> ConvertionList = new List<Entry>();
		private Config      ConvertSettings;

		private InfoBox     OutputWindow;
		private ConfigForm  ConfigWindow;

		[Serializable]
		public struct Entry
		{
			public string		Filepath;
			public string[][]	VideoStreams;
			public bool[]		VideoStreamValid;
			public string[][]	AudioStreams;
			public bool[]		AudioStreamValid;
		}

		[Serializable]
		public struct Config
		{
			// Other
			public string   OutputPath;
			public string   FFmpegPath;
			public bool     DeleteAfterFinish; // TODO : Implement | Add menu point
			public bool     ParameterToErrorOutput;
			public bool     OverrideOutput;
			public bool     ShowCMDOutput;
			public string	OutputFormat;

			// Video
			public string   VideoCodec;
			public string   VideoPreset;
			public string   VideoCustom;
			public int      VideoCRF;

			// Audio
			public string   AudioCodec;
			public string   AudioCustom;
			public int      AudioBitrate;
			public int      AudioCutoff;
			public bool     AudioStereoMixdown;

			// formats
			public string[] VideoFormats;
			public string[] AudioFormats;
		}

		private int			CurrentWorkerIndex = 0;
		private Thread		WorkerThread;
		private Process		WorkerProcess = null;
		private bool		TaskFinished;

		private bool		StartState = true;
		private bool		PauseState = false;
		private double		FileDuration;

		[DllImport( "kernel32.dll" )]
		static extern IntPtr OpenThread( int dwDesiredAccess, bool bInheritHandle, uint dwThreadId );
		[DllImport( "kernel32.dll" )]
		static extern uint SuspendThread( IntPtr hThread );
		[DllImport( "kernel32.dll" )]
		static extern int ResumeThread( IntPtr hThread );
		[DllImport( "kernel32.dll" )]
		static extern int CloseHandle( IntPtr hThread );

		public Form1()
		{
			InitializeComponent();

			string[] StateList = { "All", "Some", "None" };
			EntryVideoList.DataSource = StateList;
			EntryVideoList.ReadOnly = true;
			EntryAudioList.DataSource = StateList;
			EntryAudioList.ReadOnly = true;

			if( LoadConfig() )
				return;

			ConvertSettings.VideoCodec = "libx265";
			ConvertSettings.VideoCRF = 19;
			ConvertSettings.VideoPreset = "slow";

			ConvertSettings.AudioCodec = "libfdk_aac";
			ConvertSettings.AudioBitrate = 320;
			ConvertSettings.AudioCutoff = 20000;
			ConvertSettings.AudioStereoMixdown = false;

			ConvertSettings.AudioFormats = new string[] { "flac", "pcm_s16be", "pcm_s24le", "pcm_s16le", "dts" };
			ConvertSettings.VideoFormats = new string[] { "h264", "mpeg4" };

			ConvertSettings.OverrideOutput = true;
			ConvertSettings.FFmpegPath = "C:\\Program Files\\ffmpeg\\bin";

			ConvertSettings.OutputFormat = "mkv";
		}

		private void StoreConfig()
		{
			BinaryFormatter BinFormatter = new BinaryFormatter();
			FileStream Stream = new FileStream( "Config.bin", FileMode.Create, FileAccess.Write, FileShare.None );
			BinFormatter.Serialize( Stream, ConvertSettings );
			Stream.Close();
			// TODO : Store Video & Audio Formats
		}

		private bool LoadConfig()
		{
			if( !File.Exists( "Config.bin" ) )
				return false;

			BinaryFormatter BinFormatter = new BinaryFormatter();
			FileStream Stream = new FileStream( "Config.bin", FileMode.Open, FileAccess.Read, FileShare.Read );
			ConvertSettings = (Config)BinFormatter.Deserialize( Stream );
			Stream.Close();
			return true;
		}

		private Process CreateProcess( string Program, string Command )
		{
			Process CMDProcess = new Process();

			CMDProcess.StartInfo.FileName = Program;
			CMDProcess.StartInfo.Arguments = Command;
			CMDProcess.StartInfo.CreateNoWindow = true;
			CMDProcess.StartInfo.UseShellExecute = false;
			CMDProcess.StartInfo.RedirectStandardOutput = true;
			CMDProcess.StartInfo.RedirectStandardError = true;
			CMDProcess.StartInfo.RedirectStandardInput = true;

			return CMDProcess;
		}

		private string EndProcess( Process RunningProccess )
		{
			RunningProccess.Start();
			bool NormalShutdown = false;
			int SleepCounter = 0;
			while( !NormalShutdown && SleepCounter < 1000 )
			{
				NormalShutdown = RunningProccess.WaitForExit( 100 );
				SleepCounter += 100;
			}
			string Result = RunningProccess.StandardOutput.ReadToEnd();
			if( Result == string.Empty || Result == null ) Result = RunningProccess.StandardError.ReadToEnd();
			if( !NormalShutdown && !RunningProccess.HasExited ) RunningProccess.Kill();
			return Result;
		}

		private void Thread_HandleOutput( string Message, bool error  )
		{
			if( ConvertSettings.ShowCMDOutput )
			{
				OutputMessage( Message );
			}
			Debug.Print( Message );

			// Analyze
			if( Message != null )
			{
				// TODO : Catch override questions
				if( !Message.StartsWith( "frame=" ) ) // skip start & end info
					return;

				string TimeString = Message.Substring( Message.IndexOf( "time=" ) + 5, 11 );
				double CurrentTime = TimeSpan.Parse( TimeString ).TotalMilliseconds;
				string CurrentState = ( ( CurrentTime / FileDuration ) * 100 ).ToString();
				if( CurrentState.Length > 5 )
					CurrentState = CurrentState.Substring( 0, 5 );

				if( !IsDisposed && Visible )
				{
					Invoke( new Action( () => 
					{
						infoText.Text = TimeString;
						ProgressState.Text = CurrentState + " %";
					} ) );
				}
			}
			else if( error == false ) // Finished
			{
				TaskFinished = true;
			}
		}

		private string FindStreamInfoEntry( string[] StreamInfo, string InfoName )
		{
			foreach( string Info in StreamInfo )
			{
				if( Info.StartsWith( InfoName ) )
					return Info.Substring( InfoName.Length );
			}
			return string.Empty;
		}

		private void PrepareStreamInfo( ref string[][] StreamStorage, ref bool[] StramValid, string InfoString, string[] CheckList, bool SkipFind = false )
		{
			if( InfoString != null && InfoString != string.Empty )
			{
				// TODO : Clean up StreamStorage before final store
				StreamStorage = InfoString.Split( new string[] { "[STREAM]" }, StringSplitOptions.RemoveEmptyEntries ).Select( x => x.Split( '\n' ) ).ToArray();
				if( !SkipFind )
					StramValid = new bool[StreamStorage.Length];
				for( int i = 0; i < StreamStorage.Length; ++i )
				{
					for( int j = 0; j < StreamStorage[i].Length; ++j )
						StreamStorage[i][j] = StreamStorage[i][j].TrimEnd( '\r' );

					if( !SkipFind )
					{
						string Codec = FindStreamInfoEntry( StreamStorage[i], "codec_name=" );
						StramValid[i] = Codec != string.Empty && CheckList.Contains( Codec );
					}
				}
			}
		}

		private void Thread_ConvertFile()
		{
			Entry CurrentFileData;
			// TODO : find better solution for Invokes

			var ResetInterface = new Action( () =>
			{
				startstop.Text = "Start";
				startstop.Enabled = true;
				pause.Enabled = false;
				pause.Text = "Pause";
				StartState = true;
				PauseState = false;
				infoText.Visible = false;
				TimeLength.Visible = false;
				ProgressState.Visible = false;
			} );

			var DeleteFailedEntry = new Action<string>( ( string Message ) =>
			{
				OutputMessage( Message );
				Invoke( new Action( () =>
				{
					dataGridView1.Rows.RemoveAt( CurrentWorkerIndex );
				} ) );
				ConvertionList.RemoveAt( CurrentWorkerIndex );
				CurrentWorkerIndex++;
			} );

			try
			{
				while( CurrentWorkerIndex < ConvertionList.Count )
				{
					CurrentFileData = ConvertionList[CurrentWorkerIndex];
					if( !(bool)dataGridView1.Rows[CurrentWorkerIndex].Cells[0].Value || !(
						( CurrentFileData.VideoStreams != null && CurrentFileData.VideoStreamValid.Contains( true ) ) ||
						( CurrentFileData.AudioStreams != null && CurrentFileData.AudioStreamValid.Contains( true ) ) ) )
					{
						CurrentWorkerIndex++;
						continue;
					}

					// Check for valid File
					if( !File.Exists( CurrentFileData.Filepath ) )
					{
						DeleteFailedEntry( "File doesn't exist anymore : " + CurrentFileData.Filepath );
						continue;
					}
					else // reload metadata to be sure
					{
						string VideoInfo = EndProcess( CreateProcess( ConvertSettings.FFmpegPath + "\\ffprobe.exe", "-v error -select_streams v -show_streams \"" + CurrentFileData.Filepath + "\"" ) );

						if( VideoInfo.EndsWith( "Invalid data found when processing input" ) )
						{
							DeleteFailedEntry( "File is currently invalid : " + CurrentFileData.Filepath );
							continue;
						}

						bool[] DummyStore = new bool[0];
						PrepareStreamInfo( ref CurrentFileData.VideoStreams, ref DummyStore, VideoInfo, ConvertSettings.VideoFormats, true );
					}

					{ // Get duration in Milliseconds
						FileDuration = 1;
						string Duration = EndProcess(CreateProcess( ConvertSettings.FFmpegPath + "\\ffprobe.exe", "-v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 \"" + CurrentFileData.Filepath + "\"" ) ).TrimEnd( '\r', '\n' );
						if( Duration != null && Duration != string.Empty )
						{
							int SearchResult = Duration.IndexOf( '.' );
							if( SearchResult != -1 && SearchResult + 4 <= Duration.Length )
							{
								Duration = Duration.Substring( 0, SearchResult + 4 );
								FileDuration = Convert.ToDouble( Duration );
							}
						}
					}
					// Block active row
					Invoke( new Action( () =>
					{
						DataGridViewRow CurrentRow = dataGridView1.Rows[CurrentWorkerIndex];
						CurrentRow.Cells[0].ReadOnly = true; // Enable
						CurrentRow.Cells[2].ReadOnly = true; // Video
						CurrentRow.Cells[3].ReadOnly = true; // Audio
						CurrentRow.DefaultCellStyle.BackColor = Color.YellowGreen;
						// Set duration
						string FullFileTimeString = TimeSpan.FromMilliseconds( FileDuration ).ToString();
						if( FullFileTimeString.Length > 11 )
							FullFileTimeString = FullFileTimeString.Substring( 0, 11 );
						TimeLength.Text = "/ " + FullFileTimeString + " |";
					} ) );

					string Parameter = ( ConvertSettings.OverrideOutput )?( "-y " ):( string.Empty );
					
					Parameter += "-i \"" + CurrentFileData.Filepath + "\" -map 0 -c copy ";
					// calculate parameters
					{
						string FullOutputPath;
						// TODO : test custom outputpat | change file to mkv
						if( ConvertSettings.OutputPath != null && ConvertSettings.OutputPath != string.Empty )
							FullOutputPath = ConvertSettings.OutputPath + CurrentFileData.Filepath.Substring( CurrentFileData.Filepath.LastIndexOf( '\\' ) );
						else
							FullOutputPath = CurrentFileData.Filepath.Substring( 0, CurrentFileData.Filepath.LastIndexOf( '.' ) ) + "_final." + ConvertSettings.OutputFormat;

						if( CurrentFileData.AudioStreamValid != null )
						{
							for( int i = 0; i < CurrentFileData.AudioStreamValid.Length; ++i )
							{
								if( !CurrentFileData.AudioStreamValid[i] ) continue;

								Parameter += "-c:a:" + i + " " + ConvertSettings.AudioCodec + " -b:a " + ConvertSettings.AudioBitrate + "k ";
								if( ConvertSettings.AudioCutoff > 0 )
									Parameter += "-cutoff " + ConvertSettings.AudioCutoff + " ";
								if( ConvertSettings.AudioStereoMixdown && Convert.ToInt32( FindStreamInfoEntry( CurrentFileData.AudioStreams[i], "channels=" ) ) > 2 )
									Parameter += "-ac 2 ";
								Parameter += ConvertSettings.AudioCustom + " ";
							}
						}

						if( CurrentFileData.VideoStreamValid != null )
						{
							for( int i = 0; i < CurrentFileData.VideoStreamValid.Length; ++i )
							{
								if( !CurrentFileData.VideoStreamValid[i] ) continue;

								Parameter += "-c:v:" + i + " " + ConvertSettings.VideoCodec + " -preset " + ConvertSettings.VideoPreset + " -crf " + ConvertSettings.VideoCRF + " " + ConvertSettings.VideoCustom + " ";
							}
						}
						Parameter += "\"" + FullOutputPath + "\"";
					}
					Debug.Print( Parameter );
					if( ConvertSettings.ParameterToErrorOutput ) OutputMessage( Parameter );

					WorkerProcess = CreateProcess( ConvertSettings.FFmpegPath + "\\ffmpeg.exe", Parameter );
					WorkerProcess.OutputDataReceived += new DataReceivedEventHandler( ( o, e ) => { Thread_HandleOutput( e.Data, false ); } );
					WorkerProcess.ErrorDataReceived += new DataReceivedEventHandler( ( o, e ) => { Thread_HandleOutput( e.Data, true ); } );
					WorkerProcess.Start();
					WorkerProcess.BeginOutputReadLine();
					WorkerProcess.BeginErrorReadLine();

					while( !TaskFinished )
					{
						Thread.Sleep( 100 ); // used instead of waitforexit
					}
					Debug.Print( "Finish Task" );
					TaskFinished = false;

					if( ConvertSettings.DeleteAfterFinish )
					{
						// TODO : find reliable solution to check for errors while conversion
						Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile( CurrentFileData.Filepath, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin );
					}

					Invoke( new Action( () => // mark row as complete
					{
						DataGridViewRow CurrentRow = dataGridView1.Rows[CurrentWorkerIndex];
						CurrentRow.Cells[0].Selected = false; // prevent update block while selected
						CurrentRow.Cells[0].Value = false;
						CurrentRow.DefaultCellStyle.BackColor = Color.DarkGray;
					} ) );

					// clear entry to save space
					CurrentFileData.AudioStreams = null;
					CurrentFileData.AudioStreamValid = null;
					CurrentFileData.VideoStreams = null;
					CurrentFileData.VideoStreamValid = null;
					CurrentFileData.Filepath = null;

					CurrentWorkerIndex++;
				}
				WorkerProcess = null;
			}
			catch( ThreadAbortException )
			{
				if( WorkerProcess != null )
				{
					if( PauseState )
						UnpauseProcess();

					WorkerProcess.StandardInput.Write( 'q' );
					if( WorkerProcess.WaitForExit( 10000 ) )
						Debug.Print( "CMD closed normally" );
					else
						WorkerProcess.Kill();

					WorkerProcess = null;
				}

				if( !IsDisposed )
				{
					Invoke( ResetInterface + new Action( () =>
					{
						DataGridViewRow CurrentRow = dataGridView1.Rows[CurrentWorkerIndex];
						CurrentRow.ReadOnly = false;
						CurrentRow.DefaultCellStyle.BackColor = Color.White;
					} ) );
				}
				Debug.Print( "CleanUp Complete" );
				WorkerThread = null;
			}
			Invoke( ResetInterface );
			WorkerThread = null;
		}

		// file(s) drop
		private void dataGridView1_DragDrop( object sender, DragEventArgs e )
		{
			Entry Data = new Entry();
			string[] InputData = (string[])e.Data.GetData( DataFormats.FileDrop );
			foreach( string FileData in InputData )
			{
				Data.Filepath = FileData;
				// TODO : Check if this file is already in List
				string VideoInfo = EndProcess( CreateProcess( ConvertSettings.FFmpegPath + "\\ffprobe.exe", "-v error -select_streams v -show_streams \"" + Data.Filepath + "\"" ) );

				if( VideoInfo.EndsWith( "Invalid data found when processing input\r\n" ) )
				{
					OutputMessage( "File is invalid : " + FileData );
					continue;
				}
				// Check for valid Video format
				PrepareStreamInfo( ref Data.VideoStreams, ref Data.VideoStreamValid, VideoInfo, ConvertSettings.VideoFormats );
				bool ValidVideo = ( Data.VideoStreamValid != null && Data.VideoStreamValid.Contains( true ) );
				// Check for valid Audio format
				string AudioInfo = EndProcess( CreateProcess( ConvertSettings.FFmpegPath + "\\ffprobe.exe", "-v error -select_streams a -show_streams \"" + Data.Filepath + "\"" ) );
				PrepareStreamInfo( ref Data.AudioStreams, ref Data.AudioStreamValid, AudioInfo, ConvertSettings.AudioFormats );
				bool ValidAudio = ( Data.AudioStreamValid != null && Data.AudioStreamValid.Contains( true ) );

				dataGridView1.Rows.Add( ValidVideo || ValidAudio, Data.Filepath, GetCheckState( Data.VideoStreamValid ), GetCheckState( Data.AudioStreamValid ) );

				ConvertionList.Add( Data );		
			}
		}
		
		private void dataGridView1_DragEnter( object sender, DragEventArgs e )
		{
			if( e.Data.GetDataPresent( DataFormats.FileDrop ) )
				e.Effect = DragDropEffects.Move;
		}

		private void dataGridView1_CurrentCellDirtyStateChanged( object sender, EventArgs e )
		{
			dataGridView1.CommitEdit( DataGridViewDataErrorContexts.Commit );
		}

		private void startstop_Click( object sender, EventArgs e )
		{
			if( StartState )
			{
				if( ConvertionList.Count > 0 )
				{
					WorkerThread = new Thread( new ThreadStart( Thread_ConvertFile ) );
					// Setup
					TaskFinished = false;
					CurrentWorkerIndex = 0;
					WorkerThread.Start();
					startstop.Text = "Stop";
					pause.Enabled = true;
					StartState = false;
					infoText.Visible = true;
					TimeLength.Visible = true;
					ProgressState.Visible = true;
				}
			}
			else
			{
				// TODO : Check for running
				WorkerThread.Abort();

				startstop.Text = "Start";
				pause.Enabled = false;
				pause.Text = "Pause";
				StartState = true;
				startstop.Enabled = false; // prevent using start before process finished to abort
				infoText.Visible = false;
				TimeLength.Visible = false;
				ProgressState.Visible = false;
			}
		}

		private void pause_Click( object sender, EventArgs e )
		{
			Debug.Print( "Pause clicked" );
			if( PauseState )
			{
				UnpauseProcess();
				pause.Text = "Pause";
				PauseState = false;
			}
			else
			{
				PauseProcess();
				pause.Text = "Unpause";
				PauseState = true;
			}
		}

		private void PauseProcess()
		{
			foreach( ProcessThread thread in WorkerProcess.Threads )
			{
				IntPtr pOpenThread = OpenThread( 0x0002, false, (uint)thread.Id );
				if( pOpenThread == IntPtr.Zero )
					continue;
				SuspendThread( pOpenThread );
				CloseHandle( pOpenThread );
			}
		}

		private void UnpauseProcess()
		{
			foreach( ProcessThread thread in WorkerProcess.Threads )
			{
				IntPtr pOpenThread = OpenThread( 0x0002, false, (uint)thread.Id );
				if( pOpenThread == IntPtr.Zero )
					continue;
				int SuspendCount = 0;
				do
				{
					SuspendCount = ResumeThread( pOpenThread );
				} while( SuspendCount > 0 );

				CloseHandle( pOpenThread );
			}
		}

		private void Form1_FormClosed( object sender, FormClosedEventArgs e )
		{
			if( WorkerThread != null )
				WorkerThread.Abort(); // stop running processes

			StoreConfig();
		}

		private void dataGridView1_CellClick( object sender, DataGridViewCellEventArgs e )
		{
			if( dataGridView1.CurrentRow == null || e.RowIndex < 0 || dataGridView1.CurrentRow.ReadOnly )
			{
				DropdownListBox.Visible = false;
				return;
			}

			if( e.ColumnIndex == 2 || e.ColumnIndex == 3 )
			{
				int StreamCounter = 0;
				
				// fill dropdown with file specific data
				Entry Data = ConvertionList[e.RowIndex];
				DropdownListBox.Items.Clear();
				DropdownListBox.Visible = false; // prevent empty listbox

				if( e.ColumnIndex == 2 )
				{
					if( Data.VideoStreams == null )
						return;

					StreamCounter = Data.VideoStreams.Length;
					for( int i = 0; i < StreamCounter; ++i )
						DropdownListBox.Items.Add( "Stream " + i, Data.VideoStreamValid[i] );
				}
				else
				{
					if( Data.AudioStreams == null )
						return;

					StreamCounter = Data.AudioStreams.Length;
					for( int i = 0; i < StreamCounter; ++i )
						DropdownListBox.Items.Add( "Stream " + i, Data.AudioStreamValid[i] );
				}

				// set dropdown position
				Rectangle rec = dataGridView1.GetCellDisplayRectangle( e.ColumnIndex, e.RowIndex, true );
				DropdownListBox.Location = rec.Location + new Size( 0, 1 );
				DropdownListBox.Width = rec.Width;
				DropdownListBox.Height = rec.Height * StreamCounter;

				DropdownListBox.Visible = true;
			}
			else if( e.ColumnIndex == 4 )
				InfoButton_Click( e.RowIndex );
			else
				DropdownListBox.Visible = false;
		}

		// show entry info
		private void InfoButton_Click( int Index )
		{
			InfoBox FileInfo = new InfoBox( ConvertionList[Index] );
			FileInfo.Show();
		}

		private void DropdownListBox_ItemCheck( object sender, ItemCheckEventArgs e )
		{
			Point RowCell = dataGridView1.CurrentCellAddress;
			Entry Data = ConvertionList[RowCell.Y];
			bool NewValue = e.NewValue == CheckState.Checked;

			if( RowCell.X == 2 )
			{
				Data.VideoStreamValid[e.Index] = NewValue;
				dataGridView1.CurrentCell.Value = GetCheckState( Data.VideoStreamValid );
			}
			else
			{
				Data.AudioStreamValid[e.Index] = NewValue;
				dataGridView1.CurrentCell.Value = GetCheckState( Data.AudioStreamValid );
			}
		}

		private string GetCheckState( bool[] StateArray )
		{
			if( StateArray != null )
			{
				int TrueCounter = 0;
				foreach( bool State in StateArray )
				{
					if( State ) TrueCounter++;
				}
				if( TrueCounter == StateArray.Length ) return "All";
				else if( TrueCounter > 0 ) return "Some";
			}
			return "None";
		}

		// remove entry from list
		private void dataGridView1_KeyUp( object sender, KeyEventArgs e )
		{
			if( ConvertionList.Count == 0 || dataGridView1.CurrentRow == null )
				return;

			if( e.KeyCode == Keys.Delete && dataGridView1.CurrentRow.DefaultCellStyle.BackColor != Color.YellowGreen )
			{
				if( dataGridView1.CurrentRow.Index < CurrentWorkerIndex )
					CurrentWorkerIndex--;

				ConvertionList.RemoveAt( dataGridView1.CurrentRow.Index );
				dataGridView1.Rows.RemoveAt( dataGridView1.CurrentRow.Index );
			}
		}

		private void OutputMessage( string Message )
		{
			if( OutputWindow == null || OutputWindow.IsDisposed )
				OutputWindow = new InfoBox();

			Action OutputError = new Action( () =>
			{
				OutputWindow.Show();
				OutputWindow.AddErrorLine( Message );
			} );

			if( IsDisposed ) // prevent Form-disposed while 'OutputError' is created
				return;

			if( InvokeRequired )
				Invoke( OutputError );
			else
				OutputError();
		}

		// show config
		private void configToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if( ConfigWindow == null || ConfigWindow.IsDisposed )
				ConfigWindow = new ConfigForm( this, ConvertSettings );

			ConfigWindow.Show();
			ConfigWindow.Focus();
		}

		public void StoreNewSettings( Config NewSettings )
		{
			Invoke( new Action( () =>
			{
				ConvertSettings = NewSettings;
			} ) );
		}

		private void saveListToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if( ConvertionList.Count == 0 )
				return;

			if( saveFileDialog1.ShowDialog() == DialogResult.OK )
			{
				BinaryFormatter BinFormatter = new BinaryFormatter();
				FileStream Stream = new FileStream( saveFileDialog1.FileName, FileMode.Create, FileAccess.Write, FileShare.None );
				BinFormatter.Serialize( Stream, ConvertionList );
				Stream.Close();
			}
		}

		private void loadListToolStripMenuItem_Click( object sender, EventArgs e )
		{
			if( StartState == false )
			{
				MessageBox.Show( "You cant load a list while a the converter is running/paused" );
				return;
			}

			if( openFileDialog1.ShowDialog() == DialogResult.OK )
			{
				BinaryFormatter BinFormatter = new BinaryFormatter();
				FileStream Stream = new FileStream( openFileDialog1.FileName, FileMode.Open, FileAccess.Read, FileShare.Read );
				ConvertionList = (List<Entry>)BinFormatter.Deserialize( Stream );
				Stream.Close();
				dataGridView1.Rows.Clear();
				foreach( Entry ListEntry in ConvertionList )
				{
					dataGridView1.Rows.Add( ListEntry.AudioStreamValid.Contains( true ) || ListEntry.VideoStreamValid.Contains( true ), ListEntry.Filepath, GetCheckState( ListEntry.VideoStreamValid ), GetCheckState( ListEntry.AudioStreamValid ) );
				}
			}
		}
	}
}
