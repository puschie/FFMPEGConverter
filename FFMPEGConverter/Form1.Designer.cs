﻿namespace FFMPEGConverter
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.EntryEnabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.EntryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.EntryVideoList = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.EntryAudioList = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.EntryInfo = new System.Windows.Forms.DataGridViewButtonColumn();
			this.startstop = new System.Windows.Forms.Button();
			this.pause = new System.Windows.Forms.Button();
			this.infoText = new System.Windows.Forms.Label();
			this.DropdownListBox = new System.Windows.Forms.CheckedListBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.generalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.ProgressState = new System.Windows.Forms.Label();
			this.TimeLength = new System.Windows.Forms.Label();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowDrop = true;
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToOrderColumns = true;
			this.dataGridView1.AllowUserToResizeRows = false;
			this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EntryEnabled,
            this.EntryName,
            this.EntryVideoList,
            this.EntryAudioList,
            this.EntryInfo});
			this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView1.Location = new System.Drawing.Point(0, 0);
			this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.Size = new System.Drawing.Size(701, 517);
			this.dataGridView1.TabIndex = 2;
			this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
			this.dataGridView1.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView1_CurrentCellDirtyStateChanged);
			this.dataGridView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragDrop);
			this.dataGridView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragEnter);
			this.dataGridView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyUp);
			// 
			// EntryEnabled
			// 
			this.EntryEnabled.HeaderText = "Enabled";
			this.EntryEnabled.MinimumWidth = 25;
			this.EntryEnabled.Name = "EntryEnabled";
			this.EntryEnabled.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.EntryEnabled.Width = 50;
			// 
			// EntryName
			// 
			this.EntryName.HeaderText = "Filename";
			this.EntryName.MinimumWidth = 50;
			this.EntryName.Name = "EntryName";
			this.EntryName.ReadOnly = true;
			this.EntryName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.EntryName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.EntryName.Width = 200;
			// 
			// EntryVideoList
			// 
			this.EntryVideoList.HeaderText = "Video";
			this.EntryVideoList.Items.AddRange(new object[] {
            "NONE"});
			this.EntryVideoList.MinimumWidth = 100;
			this.EntryVideoList.Name = "EntryVideoList";
			// 
			// EntryAudioList
			// 
			this.EntryAudioList.HeaderText = "Audio";
			this.EntryAudioList.Items.AddRange(new object[] {
            "NONE"});
			this.EntryAudioList.MinimumWidth = 100;
			this.EntryAudioList.Name = "EntryAudioList";
			this.EntryAudioList.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			// 
			// EntryInfo
			// 
			this.EntryInfo.HeaderText = "Infos";
			this.EntryInfo.MinimumWidth = 50;
			this.EntryInfo.Name = "EntryInfo";
			this.EntryInfo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.EntryInfo.Width = 50;
			// 
			// startstop
			// 
			this.startstop.Location = new System.Drawing.Point(4, 5);
			this.startstop.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.startstop.Name = "startstop";
			this.startstop.Size = new System.Drawing.Size(97, 52);
			this.startstop.TabIndex = 3;
			this.startstop.Text = "Start";
			this.startstop.UseVisualStyleBackColor = true;
			this.startstop.Click += new System.EventHandler(this.startstop_Click);
			// 
			// pause
			// 
			this.pause.Enabled = false;
			this.pause.Location = new System.Drawing.Point(109, 5);
			this.pause.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.pause.Name = "pause";
			this.pause.Size = new System.Drawing.Size(99, 52);
			this.pause.TabIndex = 4;
			this.pause.Text = "Pause";
			this.pause.UseVisualStyleBackColor = true;
			this.pause.Click += new System.EventHandler(this.pause_Click);
			// 
			// infoText
			// 
			this.infoText.AutoSize = true;
			this.infoText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.infoText.Location = new System.Drawing.Point(213, 10);
			this.infoText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.infoText.Name = "infoText";
			this.infoText.Size = new System.Drawing.Size(84, 17);
			this.infoText.TabIndex = 5;
			this.infoText.Text = "00:00:00.00";
			this.infoText.Visible = false;
			// 
			// DropdownListBox
			// 
			this.DropdownListBox.CheckOnClick = true;
			this.DropdownListBox.FormattingEnabled = true;
			this.DropdownListBox.Location = new System.Drawing.Point(204, 80);
			this.DropdownListBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.DropdownListBox.Name = "DropdownListBox";
			this.DropdownListBox.Size = new System.Drawing.Size(176, 123);
			this.DropdownListBox.TabIndex = 6;
			this.DropdownListBox.Visible = false;
			this.DropdownListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.DropdownListBox_ItemCheck);
			// 
			// menuStrip1
			// 
			this.menuStrip1.ImageScalingSize = new System.Drawing.Size(19, 19);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalToolStripMenuItem,
            this.configToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
			this.menuStrip1.Size = new System.Drawing.Size(701, 31);
			this.menuStrip1.TabIndex = 7;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// generalToolStripMenuItem
			// 
			this.generalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveListToolStripMenuItem,
            this.loadListToolStripMenuItem});
			this.generalToolStripMenuItem.Name = "generalToolStripMenuItem";
			this.generalToolStripMenuItem.Size = new System.Drawing.Size(81, 27);
			this.generalToolStripMenuItem.Text = "General";
			// 
			// saveListToolStripMenuItem
			// 
			this.saveListToolStripMenuItem.Name = "saveListToolStripMenuItem";
			this.saveListToolStripMenuItem.Size = new System.Drawing.Size(153, 28);
			this.saveListToolStripMenuItem.Text = "Save List";
			this.saveListToolStripMenuItem.Click += new System.EventHandler(this.saveListToolStripMenuItem_Click);
			// 
			// loadListToolStripMenuItem
			// 
			this.loadListToolStripMenuItem.Name = "loadListToolStripMenuItem";
			this.loadListToolStripMenuItem.Size = new System.Drawing.Size(153, 28);
			this.loadListToolStripMenuItem.Text = "Load List";
			this.loadListToolStripMenuItem.Click += new System.EventHandler(this.loadListToolStripMenuItem_Click);
			// 
			// configToolStripMenuItem
			// 
			this.configToolStripMenuItem.Name = "configToolStripMenuItem";
			this.configToolStripMenuItem.Size = new System.Drawing.Size(72, 27);
			this.configToolStripMenuItem.Text = "Config";
			this.configToolStripMenuItem.Click += new System.EventHandler(this.configToolStripMenuItem_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.splitContainer1.Location = new System.Drawing.Point(0, 31);
			this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.DropdownListBox);
			this.splitContainer1.Panel1.Controls.Add(this.dataGridView1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.ProgressState);
			this.splitContainer1.Panel2.Controls.Add(this.TimeLength);
			this.splitContainer1.Panel2.Controls.Add(this.startstop);
			this.splitContainer1.Panel2.Controls.Add(this.pause);
			this.splitContainer1.Panel2.Controls.Add(this.infoText);
			this.splitContainer1.Size = new System.Drawing.Size(701, 586);
			this.splitContainer1.SplitterDistance = 517;
			this.splitContainer1.SplitterWidth = 5;
			this.splitContainer1.TabIndex = 8;
			// 
			// ProgressState
			// 
			this.ProgressState.AutoSize = true;
			this.ProgressState.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ProgressState.Location = new System.Drawing.Point(393, 10);
			this.ProgressState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.ProgressState.Name = "ProgressState";
			this.ProgressState.Size = new System.Drawing.Size(61, 17);
			this.ProgressState.TabIndex = 7;
			this.ProgressState.Text = "00,00 %";
			this.ProgressState.Visible = false;
			// 
			// TimeLength
			// 
			this.TimeLength.AutoSize = true;
			this.TimeLength.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TimeLength.Location = new System.Drawing.Point(293, 10);
			this.TimeLength.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.TimeLength.Name = "TimeLength";
			this.TimeLength.Size = new System.Drawing.Size(99, 17);
			this.TimeLength.TabIndex = 6;
			this.TimeLength.Text = "/ 00:00:00.00 |";
			this.TimeLength.Visible = false;
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			this.openFileDialog1.Filter = "Converter File Liste|*.cfl";
			// 
			// saveFileDialog1
			// 
			this.saveFileDialog1.Filter = "Converter File Liste|*.cfl";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(701, 617);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Name = "Form1";
			this.Text = "Converter";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Button startstop;
		private System.Windows.Forms.Button pause;
		private System.Windows.Forms.Label infoText;
		private System.Windows.Forms.DataGridViewCheckBoxColumn EntryEnabled;
		private System.Windows.Forms.DataGridViewTextBoxColumn EntryName;
		private System.Windows.Forms.DataGridViewComboBoxColumn EntryVideoList;
		private System.Windows.Forms.DataGridViewComboBoxColumn EntryAudioList;
		private System.Windows.Forms.DataGridViewButtonColumn EntryInfo;
		private System.Windows.Forms.CheckedListBox DropdownListBox;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem generalToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveListToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem loadListToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Label ProgressState;
		private System.Windows.Forms.Label TimeLength;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
	}
}

