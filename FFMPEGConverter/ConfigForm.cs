﻿using System;
using System.IO;
using System.Windows.Forms;

namespace FFMPEGConverter
{
	public partial class ConfigForm : Form
	{
		private Form1.Config	ChangedConfig;
		private Form1			Reference;

		public ConfigForm( Form1 Ref, Form1.Config Settings )
		{
			InitializeComponent();

			Reference = Ref;
			ChangedConfig = Settings;

			// Init with Current Config
			ffmpegPath.Text = Settings.FFmpegPath;
			outputPath.Text = Settings.OutputPath;
			deleteFile.Checked = Settings.DeleteAfterFinish;
			parameterOutput.Checked = Settings.ParameterToErrorOutput;
			overrideFile.Checked = Settings.OverrideOutput;
			videoCodec.SelectedIndex = videoCodec.Items.IndexOf( Settings.VideoCodec );
			videoPreset.SelectedIndex = videoPreset.Items.IndexOf( Settings.VideoPreset );
			videoCRF.Value = Settings.VideoCRF;
			videoCustom.Text = Settings.VideoCustom;
			audioCodec.SelectedIndex = audioCodec.Items.IndexOf( Settings.AudioCodec );
			audioBitrate.Value = Settings.AudioBitrate;
			audioCutoff.Value = Settings.AudioCutoff;
			audioCustom.Text = Settings.AudioCustom;
			audioMixdown.Checked = Settings.AudioStereoMixdown;
			cmdOutput.Checked = Settings.ShowCMDOutput;

			if( Settings.OutputFormat == null )
				outputFormat.SelectedIndex = 0;
			else
				outputFormat.SelectedIndex = outputFormat.Items.IndexOf( Settings.OutputFormat );

			videoFormats.Text = string.Join( Environment.NewLine, Settings.VideoFormats );
			audioFormats.Text = string.Join( Environment.NewLine, Settings.AudioFormats );
		}

		private void noButton_Click( object sender, EventArgs e )
		{
			Close();
		}
		private void yesButton_Click( object sender, EventArgs e )
		{
			string[] Splits = { Environment.NewLine };
			ChangedConfig.VideoFormats = videoFormats.Text.Split( Splits, StringSplitOptions.RemoveEmptyEntries );
			ChangedConfig.AudioFormats = audioFormats.Text.Split( Splits, StringSplitOptions.RemoveEmptyEntries );
			Reference.StoreNewSettings( ChangedConfig );
			Close();
		}

		private void ffmpegPath_Click( object sender, EventArgs e )
		{
			folderBrowserDialog1.SelectedPath = ChangedConfig.FFmpegPath;

			if( folderBrowserDialog1.ShowDialog() == DialogResult.OK )
			{
				if( !File.Exists( folderBrowserDialog1.SelectedPath + "\\ffmpeg.exe" ) )
				{
					MessageBox.Show( "ffmpeg.exe was not found" );
					return;
				}
				if( !File.Exists( folderBrowserDialog1.SelectedPath + "\\ffprobe.exe" ) )
				{
					MessageBox.Show( "ffprobe.exe was not found" );
					return;
				}
				ffmpegPath.Text = folderBrowserDialog1.SelectedPath;
				ChangedConfig.FFmpegPath = folderBrowserDialog1.SelectedPath;
			}
		}

		private void outputPath_Click( object sender, EventArgs e )
		{
			if( folderBrowserDialog1.ShowDialog() == DialogResult.OK )
			{
				ChangedConfig.OutputPath = folderBrowserDialog1.SelectedPath;
				outputPath.Text = folderBrowserDialog1.SelectedPath;
			}
		}

		private void deleteFile_CheckedChanged( object sender, EventArgs e )
		{
			ChangedConfig.DeleteAfterFinish = deleteFile.Checked;
		}

		private void parameterOutput_CheckedChanged( object sender, EventArgs e )
		{
			ChangedConfig.ParameterToErrorOutput = parameterOutput.Checked;
		}

		private void overrideFile_CheckedChanged( object sender, EventArgs e )
		{
			ChangedConfig.OverrideOutput = overrideFile.Checked;
		}

		private void videoCodec_SelectedIndexChanged( object sender, EventArgs e )
		{
			ChangedConfig.VideoCodec = (string)videoCodec.Items[videoCodec.SelectedIndex];
		}

		private void videoPreset_SelectedIndexChanged( object sender, EventArgs e )
		{
			ChangedConfig.VideoPreset = (string)videoPreset.Items[videoPreset.SelectedIndex];
		}

		private void videoCRF_ValueChanged( object sender, EventArgs e )
		{
			ChangedConfig.VideoCRF = (int)videoCRF.Value;
		}

		private void videoCustom_TextChanged( object sender, EventArgs e )
		{
			ChangedConfig.VideoCustom = videoCustom.Text;
		}

		private void audioCodec_SelectedIndexChanged( object sender, EventArgs e )
		{
			ChangedConfig.AudioCodec = (string)audioCodec.Items[audioCodec.SelectedIndex];
		}

		private void audioBitrate_ValueChanged( object sender, EventArgs e )
		{
			ChangedConfig.AudioBitrate = (int)audioBitrate.Value;
		}

		private void audioCutoff_ValueChanged( object sender, EventArgs e )
		{
			ChangedConfig.AudioCutoff = (int)audioCutoff.Value;
		}

		private void audioCustom_TextChanged( object sender, EventArgs e )
		{
			ChangedConfig.AudioCustom = audioCustom.Text;
		}

		private void audioMixdown_CheckedChanged( object sender, EventArgs e )
		{
			ChangedConfig.AudioStereoMixdown = audioMixdown.Checked;
		}

		private void outputPath_KeyUp( object sender, KeyEventArgs e )
		{
			if( e.KeyCode == Keys.Delete )
			{
				ChangedConfig.OutputPath = null;
				outputPath.Text = null;
			}
		}

		private void cmdOutput_CheckedChanged( object sender, EventArgs e )
		{
			ChangedConfig.ShowCMDOutput = cmdOutput.Checked;
		}

		private void outputFormat_SelectedIndexChanged( object sender, EventArgs e )
		{
			ChangedConfig.OutputFormat = (string)outputFormat.Items[outputFormat.SelectedIndex];
		}
	}
}
